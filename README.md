- Créer un dossier Android-Workshop
- Ouvrir git bash
- Faire `cd Android-Workshop`
- Faire `git clone https://gitlab.com/jimmy.boumans/android-workshop.git`
- Faire `git pull` (Au cas où...)

Dans le cas d'un ajout (Pour mettre à jour le dépôt distant a.k.a. GitLab) :
*ATTENTION* toujours faire un git pull avant de mettre a jour le dépôt distant !!!!

- Faire : `git pull`
- Faire : `git add .`  (pour ajouter tout les fichiers à la maj)
- Faire : `git commit -m "la description de ce que j'ai modifié"`
- Faire : `git push` 