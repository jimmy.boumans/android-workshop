package com.teamApps.androidworkshop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.android.volley.RequestQueue;


public class MainActivity extends AppCompatActivity {
    TextView welcome;
    TextView app;
    TextView team;
    View fullView;
    Animation anim;

    private TextView mTextViewResult;
    private RequestQueue mQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        welcome = (TextView) findViewById(R.id.welcome);
        app = (TextView) findViewById(R.id.app);
        team = (TextView) findViewById(R.id.app);
        fullView = (View) findViewById(R.id.fullView);
        anim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fadein);
        app.startAnimation(anim);
        welcome.startAnimation(anim);
        team.startAnimation(anim);
        changeToAccueil();
        fullView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent msg = new Intent(MainActivity.this, AccueilActivity.class);
                startActivity(msg);
                return true;
            }
        });
    }

    public void changeToAccueil() {
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        Intent msg = new Intent(MainActivity.this, AccueilActivity.class);
                        startActivity(msg);
                    }

                }, 3000);



    }

}
