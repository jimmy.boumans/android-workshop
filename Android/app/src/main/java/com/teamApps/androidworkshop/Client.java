package com.teamApps.androidworkshop;

public class Client {
    private String nom;
    private int nbContact;
    private int nbMaterial;
    private String id;

    Client(String nom, int nbContact, int nbMaterial, String id){
        this.nom = nom;
        this.nbContact = nbContact;
        this.nbMaterial = nbMaterial;
        this.id = id;
    }

    Client(){

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNbContact() {
        return nbContact;
    }

    public void setNbContact(int nbContact) {
        this.nbContact = nbContact;
    }

    public int getNbMaterial() {
        return nbMaterial;
    }

    public void setNbMaterial(int nbMaterial) {
        this.nbMaterial = nbMaterial;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
