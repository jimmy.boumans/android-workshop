package com.teamApps.androidworkshop;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AccueilActivity extends AppCompatActivity {

    Context context;
    LinearLayout linearLayout;
    List<Client> listClient = new ArrayList<Client>();
    AccueilRVAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);

        context = getApplicationContext();

        String testURLClient = "http://formation.devatom.net/UDEV2/ProjetFilRouge/JSON/combined/clients_contacts.json";

//        Client amazon = new Client("amazon", 2L, 3l);
//        Client cgi = new Client("CGI", 23L, 15L);
//        Client test1 = new Client("test1", 1L, 2L);
//
//        listClient.add(amazon);
//        listClient.add(cgi);
//        listClient.add(test1);

        adapter = new AccueilRVAdapter(listClient, AccueilActivity.this);
        RecyclerView recycler = (RecyclerView) findViewById(R.id.recycler);

        recycler.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recycler.setLayoutManager(manager);
        recycler.setAdapter(adapter);
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonArrayRequest testRecuperationClient = new JsonArrayRequest(Request.Method.GET, testURLClient, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject client = response.getJSONObject(i);
                        int nbMaterial = 0;
                        if (response.getJSONObject(i).getString("clt_id").equals("1")) {
                            nbMaterial = 6;
                        }
                        int nbcontact = response.getJSONObject(i).getJSONArray("contacts").length();
                        Client clientTest = new Client(client.getString("clt_nom"), nbcontact, nbMaterial, client.getString("clt_id"));
                        Log.i("testClient", Integer.toString(nbcontact) + " " + client.getString("clt_id"));
                        listClient.add(clientTest);
                        Log.i("tableau client", listClient.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        requestQueue.add(testRecuperationClient);
        Log.e("liste Client", Integer.toString(listClient.size()));
    }
}
