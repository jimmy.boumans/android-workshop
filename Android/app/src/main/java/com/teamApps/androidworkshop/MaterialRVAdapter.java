package com.teamApps.androidworkshop;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MaterialRVAdapter extends RecyclerView.Adapter<MaterialRVAdapter.MaterialViewHolder> {
    public static class MaterialViewHolder extends RecyclerView.ViewHolder {

        TextView dn;
        TextView sn;

        MaterialViewHolder(View itemView) {


            super(itemView);
            dn = (TextView) itemView.findViewById(R.id.denomination);
            sn = (TextView) itemView.findViewById(R.id.serialNumber);
        }


    }

    List<Material> matos;

    MaterialRVAdapter(List<Material> matos) {
        this.matos = matos;
    }

    @Override
    public int getItemCount() {
        return matos.size();
    }


    @Override
    public MaterialViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_material_description, viewGroup, false);
        MaterialViewHolder mvh = new MaterialViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MaterialViewHolder materialViewHolder, int i) {
        materialViewHolder.dn.setText(materialViewHolder.dn.getText() + matos.get(i).getDenomination());
        materialViewHolder.sn.setText(materialViewHolder.sn.getText() + matos.get(i).getSerialNumber());
        materialViewHolder.dn.setTag(matos.get(i).getTypeMateriel());
    }
}
