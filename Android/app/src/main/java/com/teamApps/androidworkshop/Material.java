package com.teamApps.androidworkshop;

public class Material {
    private String denomination;
    private String serialNumber;
    private String typeMateriel;

    Material (String denomination, String serialNumber, String typeMateriel){
        this.denomination = denomination;
        this.serialNumber = serialNumber;
        this.typeMateriel = typeMateriel;
    }

    Material (String denomination){
        this.denomination = denomination;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getTypeMateriel() {
        return typeMateriel;
    }

    public void setTypeMateriel(String typeMateriel) {
        this.typeMateriel = typeMateriel;
    }
}
