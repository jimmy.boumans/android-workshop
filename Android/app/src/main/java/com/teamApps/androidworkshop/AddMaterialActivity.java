package com.teamApps.androidworkshop;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.text.TextUtils.isEmpty;

public class AddMaterialActivity extends AppCompatActivity {

    Spinner spinnerAdd;
    EditText ttDenomination;
    EditText ttSerialNumber;
    EditText ttIF;
    EditText ttIP;
    ImageButton btnCheck;
    ImageButton btnCancel;


    private boolean verification(EditText champ) {
        String verif = champ.getText().toString();
        if (verif.isEmpty() || verif == "") {
            return true;
        } else {
            Log.i("champ", verif);
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_material);


        //Affichage des possibilités de la comboBox
        spinnerAdd = (Spinner) findViewById(R.id.spinnerAdd);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.spinnerAdd, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAdd.setAdapter(adapter);

        //Action valider
        btnCheck = (ImageButton) findViewById(R.id.btnCheck);
        ttDenomination = (EditText) findViewById(R.id.ttDenomination);
        ttSerialNumber = (EditText) findViewById(R.id.ttserialNumber);
        ttIF = (EditText) findViewById(R.id.ttIF);
        ttIP = (EditText) findViewById(R.id.ttAdsIP);
        btnCheck.setOnClickListener(new View.OnClickListener() {

                                        @Override
                                        public void onClick(View view) {

                                            //récupération des valeurs des champs avec vérification préalable
                                            //ComboBox
                                            String vspinner = spinnerAdd.getSelectedItem().toString();
                                            Log.i("vspinner", vspinner);

                                            if (verification(ttDenomination) || verification(ttSerialNumber) || verification(ttIF) || verification(ttIP)) {
                                                Toast.makeText(AddMaterialActivity.this, R.string.missingField, Toast.LENGTH_SHORT).show();
                                                // Toast.makeText(getApplicationContext(), "Un champ obligatoire n'a pas été renseigné!", Toast.LENGTH_SHORT).show();
                                            } else {
                                                String vdenomination = ttDenomination.getText().toString();
                                                Log.i("vdenomination", vdenomination);

                                                String vnumeroSerie = ttSerialNumber.getText().toString();
                                                Log.i("vnumeroSerie", vnumeroSerie);

                                                String vtypeIF = ttIF.getText().toString();
                                                Log.i("vtypeIF", vtypeIF);

                                                String vadsIP = ttIP.getText().toString();
                                                Log.i("vadsIP", vadsIP);

                                                //TODO : envoyer vers API et on vide les champs que si l'envoi vers l'API a eu lieu

                                                ttDenomination.setText("");
                                                ttSerialNumber.setText("");
                                                ttIF.setText("");
                                                ttIP.setText("");

                                            }
                                        }

                                    }
        );

        //Action annuler
        btnCancel = (ImageButton) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View view) {
                                             ttDenomination.setText("");
                                             ttSerialNumber.setText("");
                                             ttIF.setText("");
                                             ttIP.setText("");
                                         }


                                     }
        );
    }
}

