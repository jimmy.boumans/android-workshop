package com.teamApps.androidworkshop;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

public class AccueilRVAdapter extends RecyclerView.Adapter<ClientViewHolder> {

    List<Client> clients;
    Context context;

    AccueilRVAdapter(List<Client> clients, Context context){
        this.clients = clients;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return clients.size();
    }

    @Override
    public ClientViewHolder onCreateViewHolder(ViewGroup viewGroup,final int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_accueil_client, viewGroup, false);
        ImageView contactIcon = (ImageView) v.findViewById(R.id.contactIcon);
        ImageView materialIcon = (ImageView) v.findViewById(R.id.materialIcon);
        ClientViewHolder cvh = new ClientViewHolder(v);
        return cvh;
    }

    @Override
    public void onBindViewHolder(final ClientViewHolder clientViewHolder, final int i) {
        clientViewHolder.clientName.setText(clients.get(i).getNom());
        clientViewHolder.nbContact.setText(Integer.toString(clients.get(i).getNbContact()));
        clientViewHolder.nbMaterial.setText(Integer.toString(clients.get(i).getNbMaterial()));
        clientViewHolder.clientName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent organization = new Intent(context, OrganizationActivity.class);
                context.startActivity(organization);
            }
        });
        clientViewHolder.contactIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contact = new Intent(context, ContactActivity.class);
                context.startActivity(contact);
            }

        });
        clientViewHolder.materialIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent material = new Intent(context , MaterialActivity.class);
                material.putExtra("client", clients.get(i).getNom());
                material.putExtra("id", clients.get(i).getId());
                context.startActivity(material);
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
