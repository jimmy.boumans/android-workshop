package com.teamApps.androidworkshop;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MaterialActivity extends AppCompatActivity {

    Spinner spinnerList;
    List<Material> listMaterial = new ArrayList<Material>();
    EditText clientName;
    Context context;

    private FloatingActionButton btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material);
        spinnerList = (Spinner) findViewById(R.id.spinnerList);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.spinnerList, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerList.setAdapter(adapter);


        final MaterialRVAdapter adapt = new MaterialRVAdapter(listMaterial);

    /*    spinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                List<Material> listTemp = new ArrayList<Material>();
                for(int j=0; j<listMaterial.size(); j++){
                    if(listMaterial.get(j).getTypeMateriel().equals(Integer.toString(i))){
                        listTemp.add(listMaterial.get(j));
                    }

                }
                listMaterial = listTemp;
                adapt.notifyDataSetChanged();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        String testURLMaterial = "http://Formation.devatom.net/UDEV2/ProjetFilRouge/JSON/exploded/netgest_materiel.json";

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonArrayRequest testRecuperation = new JsonArrayRequest(Request.Method.GET, testURLMaterial, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject matos = response.getJSONObject(i);
                        if(matos.getString("idclient").equals(getIntent().getStringExtra("id"))){
                            Material material = new Material(matos.getString("libelle"), "zbradaraldjan", matos.getString("idtype"));
                            Log.i("testMaterial", material.getDenomination());
                            listMaterial.add(material);
                            Log.i("tableauMaterial", listMaterial.toString());
                        }
                    }
                    adapt.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(testRecuperation);

        RecyclerView materialRecyclerView = (RecyclerView) findViewById(R.id.materialRecyclerView);
        materialRecyclerView.setHasFixedSize(true);
        LinearLayoutManager materialManager = new LinearLayoutManager(this);
        materialRecyclerView.setLayoutManager(materialManager);

//        Material materiel1 = new Material("HP8500Multijet", "6799766MV");
//        Material materiel2 = new Material("Canon5600GT", "976445799");
//        Material materiel3 = new Material("BrotherMFCJ35", "9745UOHG");
//
//        listMaterial.add(materiel1);
//        listMaterial.add(materiel2);
//        listMaterial.add(materiel3);

        clientName = (EditText) findViewById(R.id.organization_material);
        clientName.setText(getIntent().getStringExtra("client"));

        materialRecyclerView.setAdapter(adapt);

        btnAdd = (FloatingActionButton) findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addMaterial = new Intent(MaterialActivity.this, AddMaterialActivity.class);
                startActivity(addMaterial);
            }
        });
    }


}
