package com.teamApps.androidworkshop;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.HttpAuthHandler;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class OrganizationActivity extends AppCompatActivity {

    private TextView mTextViewResult;
    private RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organisation);

        mTextViewResult = findViewById(R.id.text_view_result);
        Button buttonParse = findViewById(R.id.button_parse);

        mQueue = Volley.newRequestQueue(this);


        /*
        Json json = new Json();
        json.jsonGET("client.json", "GET", mTextViewResult, mQueue);
        */

        /*buttonParse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Json json = new Json();
                //json.jsonGET("client.json", "GET", mTextViewResult, mQueue);
                List<JSONObject> jso = json.getJsonObjectFromIndex("client.json", 0);
                try {
                    Log.e("Debug getJsonObjectFromIndex : ", jso.get(0).getString("nom"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });*/

        buttonParse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Json jsonmat = new Json();
                jsonmat.jsonGET("typemateriel.json", "GET", mTextViewResult, mQueue);
            }
        });


    }

}
