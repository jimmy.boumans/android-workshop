package com.teamApps.androidworkshop;

import android.app.DownloadManager;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Json {

    private static final String URL_DEVATOM = "http://Formation.devatom.net/UDEV2/ProjetFilRouge/JSON/exploded/netgest_";
    private static final String ADRESSE_IP = "adresseip.json";
    private static final String APPARTIENT = "appartient.json";
    private static final String CLIENT = "client.json";
    private static final String FONCTION = "fonction.json";
    private static final String INTERFACE = "interface.json";
    private static final String MATERIEL = "materiel.json";
    private static final String PERSONNE = "personne.json";
    private static final String TYPE_AFFECTATION = "typeaffectation.json";
    private static final String TYPE_IF = "typeif.json";
    private static final String TYPE_MATERIEL = "typemateriel.json";
    private static final String VILLE = "ville.json";
    private static final String URL_DEVATOM_FULL = "http://formation.devatom.net/UDEV2/ProjetFilRouge/JSON/combined/clients_contacts.json";


    /**
     * json
     *
     * @param jsonFile
     * @param method
     * @param mTextViewResult
     * @param mQueue
     */
    protected void jsonGET(final String jsonFile, String method, final TextView mTextViewResult, final RequestQueue mQueue) {


        String url = URL_DEVATOM.concat(jsonFile);

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            //JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONArray response) {

                if (jsonFile.equalsIgnoreCase(CLIENT)) {
                    jsonParseClient(response, mTextViewResult);
                } else if (jsonFile.equalsIgnoreCase(MATERIEL)) {
                    jsonParseMateriel(response, mTextViewResult);
                } else if (jsonFile.equalsIgnoreCase(TYPE_MATERIEL)) {
                    jsonParseTypeMateriel(response, mTextViewResult);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                Log.e("[TKO] - jsonGET() ", e.toString());
            }
        });

        mQueue.add(request);

    }


    /**
     * JSON Client
     *
     * @param response
     * @param mTextViewResult
     */
    public void jsonParseClient(JSONArray response, TextView mTextViewResult) {

        try {
            JSONArray jsonArray = (JSONArray) response;
            mTextViewResult.setText(null);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject organization = jsonArray.getJSONObject(i);

                String id = organization.getString("id");
                String nom = organization.getString("nom");
                String adresse1 = organization.getString("adresse1");
                String adresse2 = organization.getString("adresse2");
                String idcpville = organization.getString("idcpville");

                mTextViewResult.append(id + " " + nom + " " + adresse1 + " " + adresse2 + " " + idcpville + "\n\n");

            }
        } catch (JSONException e) {
            Log.e("[TKO] - jsonParseClient() ", e.toString());
        }

    }

    /**
     * JSON Materiel
     *
     * @param response
     * @param mTextViewResult
     */
    public void jsonParseMateriel(JSONArray response, TextView mTextViewResult) {

        try {
            JSONArray jsonArray = (JSONArray) response;
            mTextViewResult.setText(null);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject organization = jsonArray.getJSONObject(i);

                String id = organization.getString("id");
                String libelle = organization.getString("libelle");
                String idclient = organization.getString("idclient");
                String idtype = organization.getString("idtype");

                mTextViewResult.append(id + " " + libelle + " " + idclient + " " + idtype + "\n\n");

            }
        } catch (JSONException e) {
            Log.e("[TKO] - jsonParseMateriel() ", e.toString());
        }

    }
    /**
     * JSON Type Materiel
     *
     * @param response
     * @param mTextViewResult
     */

    public void jsonParseTypeMateriel(JSONArray response, TextView mTextViewResult) {
        try {
            JSONArray jsonArray = (JSONArray) response;
            mTextViewResult.setText(null);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject typeMateriel = jsonArray.getJSONObject(i);

                String id = typeMateriel.getString("id");
                String libelle = typeMateriel.getString("libelle");

                mTextViewResult.append(id + " " + libelle + "\n\n" );
            }
        } catch (JSONException e) {
            Log.e("[TKO] - jsonParseTypeMateriel() ", e.toString());
        }

    }

    /**
     * Json object of index 'i'
     *
     * @param jsonFile
     * @param i
     */
    public List<JSONObject> getJsonObjectFromIndex(final String jsonFile, final int i) {


        String url = URL_DEVATOM.concat(jsonFile);
        final List<JSONObject> jsonObject = new ArrayList<JSONObject>();


        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            //JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONArray response) {

                JSONArray jsonArray = (JSONArray) response;
                try {
                    jsonObject.add(jsonArray.getJSONObject(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError e) {
                Log.e("[TKO] - getJsonObjectFromIndex() ", e.toString());
            }
        });

        //mQueue.add(request);
        return jsonObject;

    }

}

