package com.teamApps.androidworkshop;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ClientViewHolder extends RecyclerView.ViewHolder {
    TextView clientName;
    TextView nbContact;
    TextView nbMaterial;
    ImageView contactIcon;
    ImageView materialIcon;

    ClientViewHolder(View itemView) {
        super(itemView);
        clientName = (TextView) itemView.findViewById(R.id.clientName);
        nbContact = (TextView) itemView.findViewById(R.id.nbContact);
        nbMaterial = (TextView) itemView.findViewById(R.id.nbMaterial);
        contactIcon = (ImageView) itemView.findViewById(R.id.contactIcon);
        materialIcon = (ImageView) itemView.findViewById(R.id.materialIcon);
    }
}
