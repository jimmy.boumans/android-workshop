package com.udev2.workshop.androidWorkshopAPI.mapping;

import org.springframework.stereotype.Component;

import com.udev2.workshop.androidWorkshopAPI.dto.ClientDTO;
import com.udev2.workshop.androidWorkshopAPI.entity.ClientEntity;

@Component
public class ClientMapping {

	public ClientDTO toDto(ClientEntity entity) {
		if (entity == null) {
			return null;
		}
		
		VilleMapping villeMapping = new VilleMapping();
		ClientDTO clientDTO = new ClientDTO();

		clientDTO.setId(entity.getId());
		clientDTO.setNom(entity.getNom());
		clientDTO.setAdresse1(entity.getAdresse1());
		clientDTO.setAdresse2(entity.getAdresse2());
		clientDTO.setVille(villeMapping.toDto(entity.getVille()));

		return clientDTO;
	}

	public ClientEntity toEntity(ClientDTO dto) {
		if (dto == null) {
			return null;
		}

		VilleMapping villeMapping = new VilleMapping();
		
		ClientEntity clientEntity = new ClientEntity();

		clientEntity.setId(dto.getId());
		clientEntity.setNom(dto.getNom());
		clientEntity.setAdresse1(dto.getAdresse1());
		clientEntity.setAdresse2(dto.getAdresse2());
		clientEntity.setVille(villeMapping.toEntity(dto.getVille()));

		return clientEntity;
	}
}
