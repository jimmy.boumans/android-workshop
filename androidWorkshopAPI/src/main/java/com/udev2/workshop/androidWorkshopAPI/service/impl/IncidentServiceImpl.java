package com.udev2.workshop.androidWorkshopAPI.service.impl;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.udev2.workshop.androidWorkshopAPI.dao.IncidentDAO;
import com.udev2.workshop.androidWorkshopAPI.dto.GenericDTO;
import com.udev2.workshop.androidWorkshopAPI.dto.IncidentDTO;
import com.udev2.workshop.androidWorkshopAPI.entity.IncidentEntity;
import com.udev2.workshop.androidWorkshopAPI.mapping.IncidentMapping;
import com.udev2.workshop.androidWorkshopAPI.service.IncidentService;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@Service
@Transactional
public class IncidentServiceImpl implements IncidentService {

	@Autowired
	private IncidentDAO incidentDAO;

	@Autowired
	private IncidentMapping incidentMapping;

	@Override
	public ArrayList<IncidentDTO> findAllIncident() {

		ArrayList<IncidentEntity> incidentsEntity = incidentDAO.findAll();
		ArrayList<IncidentDTO> incidentsDTO = new ArrayList<>();

		for (int i = 0; i < incidentsEntity.size(); i++) {
			incidentsDTO.add(incidentMapping.toDto(incidentsEntity.get(i)));
		}

		return incidentsDTO;
	}

	@Override
	public IncidentDTO findById(Long id) throws Exception {

		if (incidentDAO.findById(id) == null || !incidentDAO.existsById(id)) {
			log.error("[TKO] IncidentService.findById : Id [" + id + "] inconnu");
			throw new Exception("[TKO] IncidentService.findById : Id [" + id + "] inconnu");
		}

		return incidentMapping.toDto(incidentDAO.findIncidentEntityById(id));

	}

	@Override
	public GenericDTO createIncident(IncidentDTO dto) {
		//TODO à implémenter
		return null;
	}

	@Override
	public GenericDTO updateIncident(IncidentDTO dto) {
		//TODO à implémenter
		return null;
	}
}
