package com.udev2.workshop.androidWorkshopAPI.mapping;

import org.springframework.stereotype.Component;

import com.udev2.workshop.androidWorkshopAPI.dto.MaterielDTO;
import com.udev2.workshop.androidWorkshopAPI.entity.MaterielEntity;

import lombok.Data;

@Data
@Component
public class MaterielMapping {

	public MaterielDTO toDto(MaterielEntity entity) {
		if (entity == null) {
			return null;
		}
		
		TypeMaterielMapping typeMaterielMapping = new TypeMaterielMapping();
		MaterielDTO materielDTO = new MaterielDTO();
		ClientMapping clientMapping = new ClientMapping();

		materielDTO.setId(entity.getId());
		materielDTO.setLibelle(entity.getLibelle());
		materielDTO.setNumserie(entity.getNumserie());
		materielDTO.setClient(clientMapping.toDto(entity.getClient()));
		materielDTO.setTypeMateriel(typeMaterielMapping.toDto(entity.getTypeMateriel()));

		return materielDTO;
	}

	public MaterielEntity toEntity(MaterielDTO dto) {
		if (dto == null) {
			return null;
		}

		MaterielEntity materielEntity = new MaterielEntity();
		ClientMapping clientMapping = new ClientMapping();
		TypeMaterielMapping typeMaterielMapping = new TypeMaterielMapping();

		materielEntity.setId(dto.getId());
		materielEntity.setLibelle(dto.getLibelle());
		materielEntity.setNumserie(dto.getNumserie());
		materielEntity.setClient(clientMapping.toEntity(dto.getClient()));
		materielEntity.setTypeMateriel(typeMaterielMapping.toEntity(dto.getTypeMateriel()));

		return materielEntity;
	}
}
