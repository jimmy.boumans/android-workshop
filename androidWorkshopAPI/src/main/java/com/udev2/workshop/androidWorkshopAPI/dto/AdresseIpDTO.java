package com.udev2.workshop.androidWorkshopAPI.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AdresseIpDTO {

	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("ipv4")
	private String ipv4;
	
	@JsonProperty("ipv6")
	private String ipv6;
	
	@JsonProperty("masque")
	private String masque;
	
	@JsonProperty("interface")
	private InterfaceDTO interfesse;
	
	@JsonProperty("typeAff")
	private TypeAffectationDTO typeaff;
}
