package com.udev2.workshop.androidWorkshopAPI.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.udev2.workshop.androidWorkshopAPI.entity.ClientEntity;
import com.udev2.workshop.androidWorkshopAPI.entity.MaterielEntity;

@Repository
public interface MaterielDAO extends JpaRepository<MaterielEntity, Long> {

	List<MaterielEntity> findMaterielEntityByClient(ClientEntity client);

	MaterielEntity findMaterielEntityById(Long id);
}
