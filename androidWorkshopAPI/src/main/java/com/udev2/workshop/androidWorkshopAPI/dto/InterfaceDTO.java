package com.udev2.workshop.androidWorkshopAPI.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InterfaceDTO {

	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("nom")
	private String nom;

	@JsonProperty("mac")
	private String mac;
	
	@JsonProperty("typeIf")
	private TypeIfDTO type;
	
	@JsonProperty("materiel")
	private MaterielDTO materiel;
	
}
