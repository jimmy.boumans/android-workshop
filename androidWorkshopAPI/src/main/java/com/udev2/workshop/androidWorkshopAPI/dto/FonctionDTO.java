package com.udev2.workshop.androidWorkshopAPI.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FonctionDTO {

	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("libelle")
	private String libelle;
}
