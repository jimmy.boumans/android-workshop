package com.udev2.workshop.androidWorkshopAPI.mapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.udev2.workshop.androidWorkshopAPI.dto.IncidentDTO;
import com.udev2.workshop.androidWorkshopAPI.entity.IncidentEntity;

@Component
public class IncidentMapping {

	@Autowired
	DateMapping dateMapping;

	public IncidentDTO toDto(IncidentEntity entity) {
		if (entity == null) {
			return null;
		}

		if (entity.getDateInsertion() == null || entity.getDateInsertion().toString().isEmpty()) {
			entity.setDateInsertion(dateMapping.getDate());
		}

		IncidentDTO incidentDTO = new IncidentDTO();

		incidentDTO.setId(entity.getId());
		incidentDTO.setStatus(entity.getStatus());
		incidentDTO.setMessage(entity.getMessage());
		incidentDTO.setDateInsertion(entity.getDateInsertion().toString());
		incidentDTO.setStep(entity.getStep());
		incidentDTO.setIdClient(entity.getIdClient());
		incidentDTO.setIdMateriel(entity.getIdMateriel());

		return incidentDTO;
	}

	public IncidentEntity toEntity(IncidentDTO dto) {
		if (dto == null) {
			return null;
		}

		if (dto.getDateInsertion() == null || dto.getDateInsertion().isEmpty()) {
			dto.setDateInsertion(dateMapping.getDate().toString());
		}

		IncidentEntity incidentEntity = new IncidentEntity();

		incidentEntity.setId(dto.getId());
		incidentEntity.setStatus(dto.getStatus());
		incidentEntity.setMessage(dto.getMessage());
		incidentEntity.setDateInsertion(dateMapping.stringToLocalDate(dto.getDateInsertion()));
		incidentEntity.setStep(dto.getStep());
		incidentEntity.setIdClient(dto.getIdClient());
		incidentEntity.setIdMateriel(dto.getIdMateriel());

		return incidentEntity;

	}

}
