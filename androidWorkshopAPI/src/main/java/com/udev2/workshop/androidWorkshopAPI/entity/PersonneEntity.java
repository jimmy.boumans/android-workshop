package com.udev2.workshop.androidWorkshopAPI.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "personne")
public class PersonneEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "nom", length = 50, nullable = false)
	private String nom;

	@Column(name = "prenom", length = 50, nullable = false)
	private String prenom;

	@Column(name = "telephone", length = 14)
	private String telephone;

	@Column(name = "email", length = 100, nullable = false)
	private String email;

	public PersonneEntity() {
		super();
	}

}
