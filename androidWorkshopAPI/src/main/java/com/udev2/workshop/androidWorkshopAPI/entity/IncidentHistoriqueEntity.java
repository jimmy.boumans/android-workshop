package com.udev2.workshop.androidWorkshopAPI.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "historiqueIncident")
public class IncidentHistoriqueEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "status", length = 50, nullable = false)
	private String status;
	
	@Column(name = "message", length = 300, nullable = false)
	private String message;

	@Column(name = "dateInsertion", nullable = false)
	private LocalDate dateInsertion;
	
	@Column(name = "step", nullable = false)
	private Long step;
	
	@Column(name = "idClient", nullable = false)
	private Long idClient;
	
	@Column(name = "idMateriel", nullable = false)
	private Long idMateriel;
	
	@Column(name = "idIncident", nullable = false)
	private Long idIncident;
	
	public IncidentHistoriqueEntity() {
		super();
	}
	
}
