package com.udev2.workshop.androidWorkshopAPI.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.udev2.workshop.androidWorkshopAPI.dto.MaterielDTO;
import com.udev2.workshop.androidWorkshopAPI.service.MaterielService;

import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping(value = "/materiel")
public class MaterielController {

	@Autowired
	MaterielService materielService;

	@RequestMapping(method = RequestMethod.GET, value = "/client/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ArrayList<MaterielDTO> materielClient(@PathVariable Long id) {
		return materielService.findMaterielByIdClient(id);

	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody MaterielDTO materiel(@PathVariable Long id) {

		return materielService.findById(id);

	}
}
