package com.udev2.workshop.androidWorkshopAPI.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PersonneDTO {

	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("nom")
	private String nom;
	
	@JsonProperty("prenom")
	private String prenom;
	
	@JsonProperty("telephone")
	private String telephone;
	
	@JsonProperty("email")
	private String email;
}
