package com.udev2.workshop.androidWorkshopAPI.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.udev2.workshop.androidWorkshopAPI.dto.ClientDTO;
import com.udev2.workshop.androidWorkshopAPI.dto.GenericDTO;

/**
 * Service Client
 * @author Jimmy
 *
 */
@Service
public interface ClientService {

	/**
	 * Permet de récupérer un client par son ID
	 * @return Un client
	 */
	ClientDTO findClientById(Long id);

	/**
	 * Permet de récupérer l'intégralité des clients
	 * @return une liste de ClientDTO
	 */
	ArrayList<ClientDTO> findAllClient();

	/**
	 * Permet de supprimer un client par son ID
	 * @param id
	 * @return GenericDTO
	 */
	GenericDTO deleteClientById(Long id);

	/**
	 * Permet de persister le client en entrée
	 * @param dto
	 * @return GenericDTO
	 */
	GenericDTO saveClient(ClientDTO dto);

}
