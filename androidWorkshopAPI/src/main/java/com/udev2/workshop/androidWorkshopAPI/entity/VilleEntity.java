package com.udev2.workshop.androidWorkshopAPI.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "ville")
public class VilleEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "codepostal", length = 5, nullable = false)
	private String codePostal;

	@Column(name = "ville", length = 100, nullable = false)
	private String ville;

	public VilleEntity() {
		super();
	}

}
