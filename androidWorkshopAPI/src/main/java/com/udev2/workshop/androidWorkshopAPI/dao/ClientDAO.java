package com.udev2.workshop.androidWorkshopAPI.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.udev2.workshop.androidWorkshopAPI.entity.ClientEntity;

@Repository
public interface ClientDAO extends JpaRepository<ClientEntity, Long> {
	

	/**
	 * Permet de récupérer un client par son ID
	 * @return Client
	 */
	ClientEntity findClientEntityById(Long id);  
	
	/**
	 * Permet de récupérer l'intégralité de mes clients
	 * @return ArrayList<ClientEntity>
	 */
	ArrayList<ClientEntity> findAll();

	/**
	 * Supprime un client par son ID
	 * @param id
	 */
	void deleteClientById(Long id);
	
}
