package com.udev2.workshop.androidWorkshopAPI.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.udev2.workshop.androidWorkshopAPI.dao.ClientDAO;
import com.udev2.workshop.androidWorkshopAPI.dao.MaterielDAO;
import com.udev2.workshop.androidWorkshopAPI.dto.MaterielDTO;
import com.udev2.workshop.androidWorkshopAPI.entity.ClientEntity;
import com.udev2.workshop.androidWorkshopAPI.entity.MaterielEntity;
import com.udev2.workshop.androidWorkshopAPI.mapping.MaterielMapping;
import com.udev2.workshop.androidWorkshopAPI.service.MaterielService;

import lombok.Data;

@Data
@Service
public class MaterielServiceImpl implements MaterielService {

	@Autowired
	private MaterielDAO materielDAO;
	
	@Autowired
	private ClientDAO clientDAO;

	@Autowired
	private MaterielMapping materielMapping;

	@Override
	public ArrayList<MaterielDTO> findMaterielByIdClient(Long id) {

		ArrayList<MaterielDTO> listeMaterielDTO = new ArrayList<MaterielDTO>();
		List<MaterielEntity> listeMaterielEntity = new ArrayList<MaterielEntity>();
		
		ClientEntity client = clientDAO.findClientEntityById(id);

		listeMaterielEntity = materielDAO.findMaterielEntityByClient(client);

		for (MaterielEntity mat : listeMaterielEntity) {

			MaterielDTO materielDTO = new MaterielDTO();
			materielDTO = materielMapping.toDto(mat);
			listeMaterielDTO.add(materielDTO);
		}

		return listeMaterielDTO;
	}

	@Override
	public MaterielDTO findById(Long id) {
		MaterielDTO materiel = new MaterielDTO();
		materiel = materielMapping.toDto(materielDAO.findMaterielEntityById(id));
		return materiel;
	}

}
