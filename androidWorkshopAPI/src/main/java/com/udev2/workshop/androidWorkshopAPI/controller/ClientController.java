package com.udev2.workshop.androidWorkshopAPI.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.udev2.workshop.androidWorkshopAPI.dto.ClientDTO;
import com.udev2.workshop.androidWorkshopAPI.dto.GenericDTO;
import com.udev2.workshop.androidWorkshopAPI.service.ClientService;

@RestController
@RequestMapping(value = "/client")
public class ClientController {

	@Autowired
	ClientService clientService;

	@RequestMapping(method = RequestMethod.GET, value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ArrayList<ClientDTO> client() {

		return clientService.findAllClient();

	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ClientDTO client(@PathVariable Long id) {

		return clientService.findClientById(id);

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody GenericDTO deleteClient(@PathVariable Long id) {

		return clientService.deleteClientById(id);

	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody GenericDTO saveClient(@RequestBody ClientDTO dto) {
		
		return clientService.saveClient(dto);
		
	}

}
