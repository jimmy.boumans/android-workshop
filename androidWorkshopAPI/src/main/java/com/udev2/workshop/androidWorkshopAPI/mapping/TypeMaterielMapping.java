package com.udev2.workshop.androidWorkshopAPI.mapping;

import com.udev2.workshop.androidWorkshopAPI.dto.TypeMaterielDTO;
import com.udev2.workshop.androidWorkshopAPI.entity.TypeMaterielEntity;

public class TypeMaterielMapping {

	public TypeMaterielDTO toDto(TypeMaterielEntity entity) {
		if (entity == null) {
			return null;
		}

		TypeMaterielDTO dto = new TypeMaterielDTO();

		dto.setId(entity.getId());
		dto.setLibelle(entity.getLibelle());

		return dto;
	}

	public TypeMaterielEntity toEntity(TypeMaterielDTO dto) {
		if (dto == null) {
			return null;
		}

		TypeMaterielEntity entity = new TypeMaterielEntity();

		entity.setId(dto.getId());
		entity.setLibelle(dto.getLibelle());

		return entity;
	}
}
