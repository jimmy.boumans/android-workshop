package com.udev2.workshop.androidWorkshopAPI.mapping;

import com.udev2.workshop.androidWorkshopAPI.dto.VilleDTO;
import com.udev2.workshop.androidWorkshopAPI.entity.VilleEntity;

public class VilleMapping {

	public VilleDTO toDto(VilleEntity entity) {
		if (entity == null) {
			return null;
		}

		VilleDTO dto = new VilleDTO();

		dto.setId(entity.getId());
		dto.setCodePostal(entity.getCodePostal());
		dto.setVille(entity.getVille());

		return dto;
	}

	public VilleEntity toEntity(VilleDTO dto) {
		if (dto == null) {
			return null;
		}

		VilleEntity entity = new VilleEntity();

		entity.setId(dto.getId());
		entity.setCodePostal(dto.getCodePostal());
		entity.setVille(dto.getVille());

		return entity;
	}
}
