package com.udev2.workshop.androidWorkshopAPI.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "adresseip")
public class AdresseIpEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "ipV4", length = 15, nullable = false)
	private String ipv4;

	@Column(name = "ipV6", length = 100)
	private String ipv6;

	@Column(name = "masque", length = 15, nullable = false)
	private String masque;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idinterface", nullable = false)
	private InterfaceEntity interfesse;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idtypeaff", nullable = false)
	private TypeAffectationEntity typeaff;

	public AdresseIpEntity() {
		super();
	}

}
