package com.udev2.workshop.androidWorkshopAPI.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "appartient")
public class AppartientEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "idpersonne")
	private PersonneEntity personne;
	
	@ManyToOne
	@JoinColumn(name = "idfonction")
	private FonctionEntity fonction;
	
	@ManyToOne
	@JoinColumn(name = "idclient")
	private ClientEntity client;
	
	public AppartientEntity() {
		super();
	}
	
	

}
