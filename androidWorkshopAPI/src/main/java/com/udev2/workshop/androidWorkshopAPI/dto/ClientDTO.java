package com.udev2.workshop.androidWorkshopAPI.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * DTO (Data Transfert Object)
 * @author Jimmy
 *
 */
@Data
public class ClientDTO {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("nom")
	private String nom;

	@JsonProperty("adresse1")
	private String adresse1;

	@JsonProperty("adresse2")
	private String adresse2;

	@JsonProperty("ville")
	private VilleDTO ville;

}
