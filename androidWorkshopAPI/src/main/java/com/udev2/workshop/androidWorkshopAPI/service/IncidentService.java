package com.udev2.workshop.androidWorkshopAPI.service;

import java.util.ArrayList;
import java.util.Optional;

import com.udev2.workshop.androidWorkshopAPI.dto.GenericDTO;
import com.udev2.workshop.androidWorkshopAPI.dto.IncidentDTO;

public interface IncidentService {

	/**
	 * Remonte la liste des incidents ouverts
	 * @return
	 */
	ArrayList<IncidentDTO> findAllIncident();

	/**
	 * Récupère un incident par son ID (sur la table "Incident")
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	IncidentDTO findById(Long id) throws Exception;

	/**
	 * Save new Incident
	 * @param dto
	 * @return
	 */
	GenericDTO createIncident(IncidentDTO dto);

	/**
	 * Update l'incident
	 * @param dto
	 * @return
	 */
	GenericDTO updateIncident(IncidentDTO dto);

}
