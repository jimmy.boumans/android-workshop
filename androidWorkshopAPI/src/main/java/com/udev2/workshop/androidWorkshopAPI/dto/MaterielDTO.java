package com.udev2.workshop.androidWorkshopAPI.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MaterielDTO {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("libelle")
	private String libelle;

	@JsonProperty("numserie")
	private String numserie;

	@JsonProperty("client")
	private ClientDTO client;

	@JsonProperty("typeMateriel")
	private TypeMaterielDTO typeMateriel;

}
