package com.udev2.workshop.androidWorkshopAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.udev2.workshop.androidWorkshopAPI.dto.ClientDTO;
import com.udev2.workshop.androidWorkshopAPI.dto.GenericDTO;
import com.udev2.workshop.androidWorkshopAPI.dto.IncidentDTO;
import com.udev2.workshop.androidWorkshopAPI.service.IncidentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "/incident")
public class IncidentController {

	@Autowired
	IncidentService incidentService;

	@RequestMapping(method = RequestMethod.GET, value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<IncidentDTO> findAllIncident() {

		return incidentService.findAllIncident();

	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody IncidentDTO findIncident(@PathVariable Long id) {

		try {
			return incidentService.findById(id);
		} catch (Exception e) {
			log.error("[ERROR] : " + e.toString());
		}
		return null;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody GenericDTO createIncident(@RequestBody IncidentDTO dto) {
		
		return incidentService.createIncident(dto);
		
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody GenericDTO updateIncident(@RequestBody IncidentDTO dto) {
		
		return incidentService.updateIncident(dto);
		
	}
	
	

}
