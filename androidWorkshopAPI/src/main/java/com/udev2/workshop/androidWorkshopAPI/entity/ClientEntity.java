package com.udev2.workshop.androidWorkshopAPI.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "client")
public class ClientEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "nom", length = 50, nullable = false)
	private String nom;

	@Column(name = "adresse1", length = 250)
	private String adresse1;

	@Column(name = "adresse2", length = 250)
	private String adresse2;

	@ManyToOne
	@JoinColumn(name = "idcpville")
	private VilleEntity ville;

	public ClientEntity() {
		super();
	}

}
