package com.udev2.workshop.androidWorkshopAPI.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class IncidentHistoriqueDTO {
	
	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("message")
	private String message;

	@JsonProperty("dateInsertion")
	private String dateInsertion;
	
	@JsonProperty("step")
	private Long step;
	
	@JsonProperty("idClient")
	private Long idClient;
	
	@JsonProperty("idMateriel")
	private Long idMateriel;
	
	@JsonProperty("idIncident")
	private Long idIncident;

}
