package com.udev2.workshop.androidWorkshopAPI.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.udev2.workshop.androidWorkshopAPI.dto.MaterielDTO;

@Service
public interface MaterielService {

	ArrayList<MaterielDTO> findMaterielByIdClient(Long id);

	MaterielDTO findById(Long id);
}
