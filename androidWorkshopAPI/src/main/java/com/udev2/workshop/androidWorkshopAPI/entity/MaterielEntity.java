package com.udev2.workshop.androidWorkshopAPI.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "materiel")
public class MaterielEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "libelle", length = 100, nullable = false)
	private String libelle;

	@Column(name = "numserie", length = 30, nullable = false)
	private String numserie;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idclient")
	private ClientEntity client;

	@ManyToOne
	@JoinColumn(name = "idtype")
	private TypeMaterielEntity typeMateriel;

	public MaterielEntity() {
		super();
	}

}
