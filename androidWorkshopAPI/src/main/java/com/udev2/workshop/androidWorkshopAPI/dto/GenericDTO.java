package com.udev2.workshop.androidWorkshopAPI.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class GenericDTO {
	
	@JsonProperty("code")
	private String code;
	
	@JsonProperty("message")
	private String message;

}
