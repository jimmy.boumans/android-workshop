package com.udev2.workshop.androidWorkshopAPI.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.udev2.workshop.androidWorkshopAPI.entity.IncidentEntity;

@Repository
public interface IncidentDAO extends JpaRepository<IncidentEntity, Long> {
	
	/**
	 * Permet de récupérer l'intégralité des incidents ouverts (table "Incident")
	 * @return ArrayList d'incidentEntity
	 */
	ArrayList<IncidentEntity> findAll();

	IncidentEntity findIncidentEntityById(Long id);

}
