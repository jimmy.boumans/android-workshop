package com.udev2.workshop.androidWorkshopAPI.service.impl;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.udev2.workshop.androidWorkshopAPI.dao.ClientDAO;
import com.udev2.workshop.androidWorkshopAPI.dto.ClientDTO;
import com.udev2.workshop.androidWorkshopAPI.dto.GenericDTO;
import com.udev2.workshop.androidWorkshopAPI.entity.ClientEntity;
import com.udev2.workshop.androidWorkshopAPI.mapping.ClientMapping;
import com.udev2.workshop.androidWorkshopAPI.service.ClientService;

import lombok.Data;

@Data
@Service
@Transactional
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientDAO clientDAO;

	@Autowired
	private ClientMapping clientMapping;

	@Override
	public ArrayList<ClientDTO> findAllClient() {

		ArrayList<ClientEntity> clientsEntity = clientDAO.findAll();
		ArrayList<ClientDTO> clientsDTO = new ArrayList<>();

		for (int i = 0; i < clientsEntity.size(); i++) {
			clientsDTO.add(clientMapping.toDto(clientsEntity.get(i)));
		}
		return clientsDTO;
	}

	@Override
	public ClientDTO findClientById(Long id) {
		return clientMapping.toDto(clientDAO.findClientEntityById(id));
	}

	@Override
	public GenericDTO deleteClientById(Long id) {

		GenericDTO dto = new GenericDTO();

		if (!clientDAO.existsById(id)) {
			dto.setCode("200");
			dto.setMessage("[ERROR] Le client [id = " + id + "] n'existe pas. Annulation de la suppression.");

		} else {
			clientDAO.deleteClientById(id);

			dto.setCode("0");
			dto.setMessage("[TOK] Suppression du client [id = " + id + "].");
		}

		return dto;
	}

	@Override
	public GenericDTO saveClient(ClientDTO dto) {

		GenericDTO genericDto = new GenericDTO();

		if (dto.getNom().isEmpty()) {
			genericDto.setCode("200");
			genericDto.setMessage("[TKO] - Veuillez saisir un nom valide");
		} else {
			// Set un nouvel ID
			Long id = clientDAO.count();
			dto.setId(id + 1);

			clientDAO.save(clientMapping.toEntity(dto));
			genericDto.setCode("0");
			genericDto.setMessage("[TOK] Client sauvegardé.");
		}

		return genericDto;
	}

}
