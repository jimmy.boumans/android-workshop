package com.udev2.workshop.androidWorkshopAPI.dto;

import lombok.Data;

@Data
public class VilleDTO {

	private Long id;
	
	private String codePostal;
	
	private String ville;
}
