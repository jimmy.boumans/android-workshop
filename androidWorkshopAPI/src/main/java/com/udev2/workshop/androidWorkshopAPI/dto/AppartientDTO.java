package com.udev2.workshop.androidWorkshopAPI.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AppartientDTO {

	@JsonProperty("id")
	private Long id;
	
	@JsonProperty("personne")
	private PersonneDTO personne;
	
	@JsonProperty("fonction")
	private FonctionDTO fonction;
	
	@JsonProperty("client")
	private ClientDTO client;
}
