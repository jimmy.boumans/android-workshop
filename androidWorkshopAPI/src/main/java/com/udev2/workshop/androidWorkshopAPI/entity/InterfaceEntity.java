package com.udev2.workshop.androidWorkshopAPI.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "interface")
public class InterfaceEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "nom", length = 10, nullable = false)
	private String nom;

	@Column(name = "mac", length = 14)
	private String mac;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idtype", nullable = false)
	private TypeIfEntity type;

	@ManyToOne
	@JoinColumn(name = "idmateriel")
	private MaterielEntity materiel;
	
	public InterfaceEntity() {
		super();
	}

}
