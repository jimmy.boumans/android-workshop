package com.udev2.workshop.androidWorkshopAPI.dto;

import lombok.Data;

@Data
public class TypeMaterielDTO {

	private Long id;
	
	private String libelle;
	
	public TypeMaterielDTO() {
		super();
	}
}
