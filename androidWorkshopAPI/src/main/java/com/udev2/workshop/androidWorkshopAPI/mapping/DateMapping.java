package com.udev2.workshop.androidWorkshopAPI.mapping;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

@Component
public class DateMapping {
	
	public LocalDate getDate() {

		LocalDate date = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		String text = date.format(formatter);

		return LocalDate.parse(text, formatter);

	}

	public LocalDate stringToLocalDate(String dateString) {

		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		return LocalDate.parse(dateString, dateTimeFormatter);

	}

}
