const express = require('express');
const router = express.Router();
const apiCaller = require('./apiCaller');

/* GET home page. */
router.get('/:id', (req, res, next) => {
    apiCaller.apiCall(req.message.url+req.params.id).then(data => {
        console.log(data)
        res.render(req.message.view, {
            title: 'netgest',
            data: data
        });
    }).catch(error => {
        console.log(error);
    })

});

module.exports = router;
