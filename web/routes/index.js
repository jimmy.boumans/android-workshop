const express = require('express');
const router = express.Router();
const apiCaller = require('./apiCaller');

/* GET home page. */
router.get('/', (req, res, next) => {
    res.render(req.message.view, { title: 'apiCaller' });
});

module.exports = router;
